import 'package:air_quality_app/Container_custom.dart';
// import 'package:air_quality_app/add_weather.dart';
import 'package:air_quality_app/database.dart';
// import 'package:air_quality_app/database_display_page.dart';
// import 'package:air_quality_app/display_weather.dart';
import 'package:air_quality_app/practice.dart';
import 'package:air_quality_app/practice_container.dart';
import 'package:air_quality_app/second_screen.dart';
import 'package:air_quality_app/third_screen.dart';
import 'package:air_quality_app/weather_list_page.dart';
// import 'package:air_quality_app/weather_list_page.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    MyDatabase().copyPasteAssetFileToRoot().then((value) => (value) {
      print("Database copied successfully");
    });
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool isRememberMe = true;
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var formKey = GlobalKey<FormState>();
  var showpass = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(20),
                child: const Text(
                  "Welcome Back!",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(20, 10, 20, 20),
                child: const Text(
                  "Use your credentials below and login to your account",
                  style: TextStyle(fontSize: 15),
                ),
              ),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        controller: emailController,
                        decoration:const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Email Address",
                          suffixIcon: Icon(
                            Icons.check_circle,
                            color: Colors.orange,
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty && value != null) {
                            return "Enter email address";
                          }
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: TextFormField(
                        controller: passwordController,
                        obscureText: showpass,
                        obscuringCharacter: "*",
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: "Password",
                          suffixIcon: InkWell(
                            child: Icon(showpass
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined),
                            onTap: () {
                              setState(() {
                                showpass = !showpass;
                              });
                            },
                          ),
                        ),
                        validator: (value) {
                          if (value!.isEmpty && value != null) {
                            return "Enter password";
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                children: [
                  Checkbox(
                    value: isRememberMe,
                    onChanged: (value) {
                      setState(() {
                        isRememberMe = !isRememberMe;
                      });
                      print(isRememberMe);
                    },
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        isRememberMe = !isRememberMe;
                      });
                      print(isRememberMe);
                    },
                    child:const Text(
                      "Remember me",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  const SizedBox(
                    width: 80,
                  ),
                  const Text(
                    "Forgot Password?",
                    style: TextStyle(decoration: TextDecoration.underline),
                    textAlign: TextAlign.end,
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: SizedBox(
                    width: 350.0,
                    height: 50.0,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color(0xFF4DB6AC),
                        side: BorderSide(width: 1.8),
                      ),
                      onPressed: () {
                        if (formKey.currentState!.validate()) {
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) {
                              return FContainerCustomPage();
                            }),
                          );
                        }
                      },
                      child: Text("Login my account"),
                    ),
                  ),
                ),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Or Login With",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  width: 350.0,
                  height: 50.0,
                  child: ElevatedButton.icon(
                    onPressed: () {},
                    icon: Icon(Icons.g_mobiledata),
                    label: Text("Continue with Google"),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color(0xFFFF8A80),
                      side: BorderSide(width: 1.8),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: SizedBox(
                  width: 350.0,
                  height: 50.0,
                  child: ElevatedButton.icon(
                    onPressed: () {},
                    icon: Icon(Icons.facebook),
                    label: Text("Continue with Google"),
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Color(0xFFFFE082),
                      side: BorderSide(width: 1.8),
                    ),
                  ),
                ),
              ),
              Center(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(35.0),
                      child: RichText(
                        text: const TextSpan(
                          style: TextStyle(color: Colors.black),
                          children: [
                            TextSpan(
                              text: 'by Continuing, you accept our ',
                              style: TextStyle(),
                            ),
                            TextSpan(
                              text: 'Terms and\n',
                              style: TextStyle(
                                  decoration: TextDecoration.underline),
                            ),
                            TextSpan(
                              text: '            ',
                              style: TextStyle(),
                            ),
                            TextSpan(
                              text: 'Conditions, Privacy Policy',
                              style: TextStyle(
                                  decoration: TextDecoration.underline),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
