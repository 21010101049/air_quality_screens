import 'package:flutter/material.dart';

class ContainerPracticePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    var screenHeight = MediaQuery.of(context).size.height;
    // TODO: implement build
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 5),
                      child: Column(
                        children: [
                          Card(
                            child: Container(
                              height: 40,
                              width: 40,
                              child: Image.network(
                                  "https://tse1.mm.bing.net/th?id=OIP.jt5y3tRyYCoVdWLg1DBFgQHaHa&pid=Api&rs=1&c=1&qlt=95&w=96&h=96"),
                            ),
                            elevation: 4,
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 8),
                          child: Icon(Icons.notification_add_rounded),
                        ),
                        Icon(Icons.settings)
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Hello Mikel",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 25),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(width: 2),
                          color: Colors.orange,
                        ),
                        child: TextButton.icon(
                          onPressed: () {},
                          icon: Icon(
                            Icons.add,
                            color: Colors.black,
                          ),
                          label: Text(
                            "add",
                            style: TextStyle(color: Colors.black),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Text(
                  "23 November 2022",
                  style: TextStyle(fontWeight: FontWeight.bold),
                )
              ],
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                getCustomizedContainer(screenWidth, screenHeight/3,txtheight: screenHeight/3.1),
                getCustomizedContainer(screenWidth, screenHeight/3,txtheight: screenHeight/3.1),
                getCustomizedContainer(screenWidth, screenHeight/3,txtheight: screenHeight/3.1),
                getCustomizedContainer(screenWidth, screenHeight/3,txtheight: screenHeight/3.1),
                getCustomizedContainer(screenWidth, screenHeight/3,txtheight: screenHeight/3.1),
              ],
            ),
          ),
        ],
      )
    );
  }
}

Widget getCustomizedContainer(screenWidth,height, {txtheight}){
  return Padding(
    padding: const EdgeInsets.all(8.0),
    child: Stack(
      children: [
        Center(
          child: Container(
            decoration: BoxDecoration(border: Border.all(width: 3)),
            width: screenWidth,
            height: height,
          ),
        ),
        Center(
          child: Container(
            width: screenWidth,
            height: height,
            margin: EdgeInsets.only(left: 10,right: 10,bottom: 10),
            decoration: BoxDecoration(
              // color: Colors.green,
                border: Border(bottom:BorderSide(width: 3),)
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
          child: Center(
            child: Container(
              height: txtheight,
              // padding: EdgeInsets.only(top: 100),
              margin: EdgeInsets.only(left: 10, right: 10,bottom: 5),
              decoration: BoxDecoration(
                // color: Colors.red,
                border: Border(
                  bottom: BorderSide(width: 3),
                  left: BorderSide(width: 3),
                  top: BorderSide(width: 3),
                  right: BorderSide(width: 3),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.all(8),
                          margin: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: Colors.green.shade400,
                              border: Border.all(width: 2)
                          ),
                          child: Icon(Icons.speed_outlined,size: 30,color: Colors.white,),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              // color: Colors.green,
                              width: screenWidth/1.45,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(padding: EdgeInsets.all(5),child: const Text("Jakarta",style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold),)),
                                  Row(
                                    children: [
                                      Container(child: Text("30 ",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20),)),
                                      Container(child: Text("AQI ",style: TextStyle(fontSize: 20))),
                                      Container(child: Text("USA",style: TextStyle(fontSize: 20))),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: [
                                Text("Today's air quality is "),
                                Text("Good",style: TextStyle(color: Colors.green.shade400),)
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          "-----------------------------------------------------------------------------------"),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      padding: EdgeInsets.all(8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Icon(
                                Icons.thermostat,
                                size: 30,
                              ),
                              Text(
                                "24 \u2103",
                                style: TextStyle(),
                              ),
                            ],
                          ),
                          Column(
                            children: [
                              Icon(Icons.water_drop, size: 30),
                              Text(
                                "57%",
                                style: TextStyle(),
                              )
                            ],
                          ),
                          Column(
                            children: [
                              Icon(Icons.sunny, size: 30),
                              Text(
                                "Light",
                                style: TextStyle(),
                              )
                            ],
                          ),
                          Column(
                            children: [
                              Icon(Icons.air, size: 30),
                              Text(
                                "13Km/h",
                                style: TextStyle(),
                              )
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
