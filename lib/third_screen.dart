import 'package:flutter/material.dart';

class ThirdScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(8, 8, 8, 15),
                child: Text(
                  "Ranking City",
                  style: TextStyle(fontSize: 20),
                ),
              ),
              Expanded(
                child: ListView(
                  // crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.ZA5-BMDRSP-44RnBNFArIgHaEK&pid=Api&P=0", "Lahore", Colors.pink.shade50 , "120", "Pakistan, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),
                    getCustomizedCityList(MediaQuery.of(context).size.width/1.33, "https://tse1.mm.bing.net/th?id=OIP.mj6ZxHH_YhezE3baAh0MNwHaEs&pid=Api&rs=1&c=1&qlt=95&w=170&h=107", "Delhi", Colors.pink.shade50 , "120", "India, South Asia"),

                  ],
                ),
              ),
              Stack(
                children: [
                  Container(
                    margin:
                    EdgeInsets.only(top: 6, right: 10, left: 10, bottom: 15),
                    decoration: BoxDecoration(
                      color: Colors.teal,
                      border: Border.all(width: 3),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(child: Icon(Icons.home,color: Colors.white.withOpacity(0.7),),onTap: (){
                          Navigator.of(context).pop();
                        },),
                        InkWell(child: Icon(Icons.assistant_navigation,color: Colors.white.withOpacity(0.7),),onTap: (){},),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Container(
                            padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                            // width: 100,
                            decoration: BoxDecoration(
                              color: Colors.orange[100],
                              border: Border.all(width: 3),
                            ),
                            child: Text(
                              "Rank",
                              style: TextStyle(fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        InkWell(child: Icon(Icons.network_wifi_sharp,color: Colors.white.withOpacity(0.7),),onTap: (){},)
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget getCustomizedCityList(contSize , imgUrl , cityName ,Color cityColor , cityRank , cityAddress){
  return Container(
    padding: EdgeInsets.only(bottom: 3),
    margin: EdgeInsets.only(bottom: 8),
    decoration: BoxDecoration(
      border: Border.all(width: 1),
    ),
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(width: 1),
            ),
            child: Image.network(
              imgUrl,
              fit: BoxFit.cover,
            ),
            height: 50,
            width: 50,
            margin: EdgeInsets.only(
              right: 5,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: contSize,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      cityName,
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          border: Border.all(),
                          color: cityColor,
                          borderRadius: BorderRadius.all(
                            Radius.circular(20),
                          )),
                      child: Text(cityRank),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 5,
              ),
              Text(cityAddress)
            ],
          ),
        ],
      ),
    ),
  );
}