import 'package:air_quality_app/third_screen.dart';
import 'package:flutter/material.dart';

class SecondScreenPage extends StatefulWidget {
  @override
  State<SecondScreenPage> createState() => _SecondScreenPageState();
}

class _SecondScreenPageState extends State<SecondScreenPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Column(
                          children: [
                            Card(
                              child: Container(
                                height: 40,
                                width: 40,
                                child: Image.network(
                                    "https://tse1.mm.bing.net/th?id=OIP.jt5y3tRyYCoVdWLg1DBFgQHaHa&pid=Api&rs=1&c=1&qlt=95&w=96&h=96"),
                              ),
                              elevation: 4,
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 8),
                            child: Icon(Icons.notification_add_rounded),
                          ),
                          Icon(Icons.settings)
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Hello Mikel",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 25),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(width: 2),
                            color: Colors.orange,
                          ),
                          child: TextButton.icon(
                            onPressed: () {},
                            icon: Icon(
                              Icons.add,
                              color: Colors.black,
                            ),
                            label: Text(
                              "add",
                              style: TextStyle(color: Colors.black),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "23 November 2022",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      border: Border.all(width: 5),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                  color: Colors.greenAccent,
                                  border: Border.all(width: 3),
                                ),
                                child:
                                    const Icon(Icons.speed_outlined, size: 35),
                              ),
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: Text(
                                        "Jakarta",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    // SizedBox(width: 35,),
                                    Text(
                                      "30",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      " AQI",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                    Text(
                                      " US",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Today's air quality is",
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    Text(
                                      " good",
                                      style: TextStyle(
                                          color: Colors.green, fontSize: 16),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Text(
                            "-----------------------------------------------------------------------------------"),
                        Container(
                          // color: Colors.white,
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Icon(
                                    Icons.thermostat,
                                    size: 30,
                                  ),
                                  Text(
                                    "24 \u2103",
                                    style: TextStyle(),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Icon(Icons.water_drop, size: 30),
                                  Text(
                                    "57%",
                                    style: TextStyle(),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Icon(Icons.sunny, size: 30),
                                  Text(
                                    "Light",
                                    style: TextStyle(),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Icon(Icons.air, size: 30),
                                  Text(
                                    "13Km/h",
                                    style: TextStyle(),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: AlignmentDirectional.bottomEnd,
                    child: Container(
                      margin: EdgeInsets.only(left: 2, right: 2),
                      height: 80,
                      decoration: const BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 5, color: Colors.black),
                          top: BorderSide(width: 5, color: Colors.black),
                          bottom: BorderSide(width: 5, color: Colors.black),
                          right: BorderSide(width: 5, color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.all(12),
                    decoration: BoxDecoration(
                      border: Border.all(width: 5),
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                  color: Colors.yellow,
                                  border: Border.all(width: 3),
                                ),
                                child: const Icon(Icons.speed, size: 35),
                              ),
                            ),
                            Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(right: 15),
                                      child: Text(
                                        "YogyaKarta",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    // SizedBox(width: 35,),
                                    Text(
                                      "50",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text(
                                      " AQI",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                    Text(
                                      " US",
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "Today's air quality is",
                                      style: TextStyle(fontSize: 15),
                                    ),
                                    Text(
                                      " moderate",
                                      style: TextStyle(
                                          color: Colors.orange, fontSize: 16),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        Text(
                            "-----------------------------------------------------------------------------------"),
                        Container(
                          margin: EdgeInsets.only(top: 10),
                          padding: EdgeInsets.all(8),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                children: [
                                  Icon(Icons.thermostat, size: 30),
                                  Text(
                                    "32 \u2103",
                                    style: TextStyle(),
                                  ),
                                ],
                              ),
                              Column(
                                children: [
                                  Icon(Icons.water_drop, size: 30),
                                  Text(
                                    "45%",
                                    style: TextStyle(),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Icon(Icons.sunny, size: 30),
                                  Text(
                                    "Light",
                                    style: TextStyle(),
                                  )
                                ],
                              ),
                              Column(
                                children: [
                                  Icon(Icons.air, size: 30),
                                  Text(
                                    "08Km/h",
                                    style: TextStyle(),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: AlignmentDirectional.bottomEnd,
                    child: Container(
                      margin: EdgeInsets.only(left: 2, right: 2),
                      height: 80,
                      decoration: const BoxDecoration(
                        border: Border(
                          left: BorderSide(width: 5, color: Colors.black),
                          // top: BorderSide(width: 5, color: Colors.white),
                          bottom: BorderSide(width: 5, color: Colors.black),
                          right: BorderSide(width: 5, color: Colors.black),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Stack(
              children: [
                Container(
                  margin:
                      EdgeInsets.only(top: 6, right: 10, left: 10, bottom: 15),
                  decoration: BoxDecoration(
                    color: Colors.greenAccent,
                    border: Border.all(width: 3),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Container(
                          width: 100,
                          decoration: BoxDecoration(
                            color: Colors.yellow,
                            border: Border.all(width: 3),
                          ),
                          child: Text(
                            "Home",
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      InkWell(child: Icon(Icons.assistant_navigation),onTap: (){},),
                      InkWell(child: Icon(Icons.poll_rounded),onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context){
                          print("third screen page is clicked");
                          return ThirdScreenPage();
                        }));
                      },),
                      InkWell(child: Icon(Icons.network_wifi_sharp),onTap: (){},)
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
