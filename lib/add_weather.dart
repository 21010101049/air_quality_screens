import 'package:air_quality_app/database.dart';
import 'package:air_quality_app/weather_list_page.dart';
import 'package:flutter/material.dart';

class AddWeatherPage extends StatefulWidget {
  var editUserName;
  var editCityName;
  var editTemperature;
  var editDot;
  var editId;

  AddWeatherPage(
      {this.editCityName,
      this.editId,
      this.editTemperature,
      this.editUserName});

  @override
  State<AddWeatherPage> createState() => _AddWeatherPageState();
}

class _AddWeatherPageState extends State<AddWeatherPage> {
  late TextEditingController usernameController;
  late TextEditingController citynameController;
  late TextEditingController temperatureController;
  late TextEditingController dotController;

  var editId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    usernameController = TextEditingController(text: widget.editUserName);
    citynameController = TextEditingController(text: widget.editCityName);
    temperatureController = TextEditingController(text: widget.editTemperature);
    dotController = TextEditingController(text: widget.editDot);
    editId = widget.editId ?? null;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Weather"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            TextFormField(
              controller: usernameController,
              decoration: InputDecoration(
                hintText: "Enter your name:",
                labelText: "User Name:",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            TextFormField(
              controller: citynameController,
              decoration: InputDecoration(
                hintText: "Enter your city:",
                labelText: "City Name:",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            TextFormField(
              controller: temperatureController,
              decoration: InputDecoration(
                hintText: "Enter temperature:",
                labelText: "Temperature:",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            TextFormField(
              controller: dotController,
              decoration: InputDecoration(
                hintText: "Enter date:",
                labelText: "Date:",
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            Container(
              color: Colors.blue,
              child: TextButton(
                onPressed: () {
                  print("hellooooooo:::::::::::::::::$usernameController.text");
                  MyDatabase().insertIntoWeatherTable(
                      username: usernameController.text,
                  cityname: citynameController.text,
                  temperature: temperatureController.text,
                  dot: dotController.text,
                  id: editId);
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                    return WeatherListPage();
                  },));
                },
                child: Text(
                  "Submit",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Colors.black),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
