class WeatherModel{
  late int _id;
  late String _username;
  late String _cityname;
  late String _temperature;
  late String _dot;

  String get dot => _dot;

  set dot(String dot) {
    _dot = dot;
  }

  String get temperature => _temperature;

  set temperature(String temperature) {
    _temperature = temperature;
  }

  String get cityname => _cityname;

  set cityname(String cityname) {
    _cityname = cityname;
  }

  String get username => _username;

  set username(String username) {
    _username = username;
  }

  int get id => _id;

  set id(int id) {
    _id = id;
  }

}