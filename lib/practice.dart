import 'package:flutter/material.dart';

class PracticePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            child: Container(
              decoration: BoxDecoration(
                  border: Border.all(width: 4), color: Colors.red),
            ),
            height: 500,
            width: 500,
          ),
          Positioned(
            child: Container(
              decoration: BoxDecoration(
                  border: Border(
                    left: BorderSide(width: 5, color: Colors.black),
                    // right: BorderSide(width: 5, color: Colors.white70),
                    bottom: BorderSide(width: 5, color: Colors.black),
                    top: BorderSide(width: 5, color: Colors.black),
                  ),
                  color: Colors.blue),
            ),
            height: 500,
            width: 250,
          )
        ],
      ),
    );
  }
}
