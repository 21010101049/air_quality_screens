import 'dart:io';
import 'package:air_quality_app/weather_model.dart';
import 'package:flutter/services.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase{
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'weather_forcasting.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<void> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "weather_forcasting.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
      await rootBundle.load(join('assets/database', 'weather_forcasting.db'));
      List<int> bytes =
      data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
    }
  }

  Future<List<Map<String, Object?>>> getDataFromWeatherTable() async {
    // List<WeatherModel> weatherList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data = await db.rawQuery('SELECT * FROM Weather');
    // for(int i=0;i<data.length;i++){
    //   WeatherModel model = WeatherModel();
    //   model.id = data[i]['id'] as int;
    //   model.temperature = data[i]['temperature'].toString();
    //   model.cityname = data[i]['cityname'].toString();
    //   model.username = data[i]['username'].toString();
    //   model.dot = data[i]['dot'].toString();
    //   weatherList.add(model);
    // }
    print("User Length:::${data.length}");
    return data;
  }

  Future<void> insertIntoWeatherTable({username,cityname,temperature,dot,id}) async {
    Database db = await initDatabase();
    Map<String,Object?> map = Map();
    map["username"] = username;
    map["cityname"] = cityname;
    map["temperature"] = temperature;
    map["dot"] = dot;

    if(id!=null) {
      map["id"] = id;
      await db.update("Weather", map,where: "id= ?",whereArgs: [id]);
    }
    else{
      await db.insert("Weather", map);
    }
  }

  Future<void> deleteFromTable({id}) async {
    Database db = await initDatabase();
    await db.rawQuery("delete from Weather where id=$id");
  }
}