// import 'package:air_quality_app/add_weather.dart';
import 'package:air_quality_app/add_weather.dart';
import 'package:air_quality_app/third_screen.dart';
import 'package:air_quality_app/weather_list_page.dart';
import 'package:flutter/material.dart';

class FContainerCustomPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    // TODO: implement build
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: EdgeInsets.only(bottom: 5),
                        child: Column(
                          children: [
                            Card(
                              child: Container(
                                height: 40,
                                width: 40,
                                child: Image.network(
                                    "https://tse1.mm.bing.net/th?id=OIP.jt5y3tRyYCoVdWLg1DBFgQHaHa&pid=Api&rs=1&c=1&qlt=95&w=96&h=96"),
                              ),
                              elevation: 4,
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Container(
                            height: 30,
                            width: 30,
                            margin: EdgeInsets.only(right: 8),
                            child: InkWell(
                              onTap: () {
                                print("notification icon clicked");
                              },
                              child: Icon(Icons.notification_add_outlined),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              print("settings clicked");
                            },
                            child: Image.asset(
                              "assets/icons/settings_FILL0_wght400_GRAD0_opsz48.png",
                              width: 30,
                              height: 30,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Hello Mikel",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 25),
                        ),
                        Container (
                          height: 40,
                          decoration: BoxDecoration(
                            border: Border.all(width: 2),
                            color: Colors.orange[100],
                          ),
                          child: TextButton.icon(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                return AddWeatherPage();
                              },));
                            },
                            icon: Icon(
                              Icons.add,
                              color: Colors.black,
                            ),
                            label: Text(
                              "add",
                              style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: 20),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "23 November 2022",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: [
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: getCustomStackLayout(
                        screenHeight,
                        Colors.greenAccent,
                        "Jakarta",
                        "good",
                        Colors.greenAccent,
                        "30",
                        "US"),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: getCustomStackLayout(
                        screenHeight,
                        Colors.orangeAccent,
                        "Yogyakarta",
                        "moderate",
                        Colors.orangeAccent,
                        "50",
                        "US"),
                  ),
                ],
              ),
            ),
            Stack(
              children: [
                Container(
                  margin:
                      EdgeInsets.only(top: 6, right: 10, left: 10, bottom: 15),
                  decoration: BoxDecoration(
                    color: Colors.teal,
                    border: Border.all(width: 3),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                          // width: 100,
                          decoration: BoxDecoration(
                            color: Colors.orange[100],
                            border: Border.all(width: 3),
                          ),
                          child: Text(
                            "Home",
                            style: TextStyle(fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      InkWell(
                        child: Icon(
                          Icons.assistant_navigation,
                          color: Colors.white.withOpacity(0.7),
                        ),
                        onTap: () {},
                      ),
                      InkWell(
                        child: Icon(
                          Icons.poll_rounded,
                          color: Colors.white.withOpacity(0.7),
                        ),
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            print("third screen page is clicked");
                            return ThirdScreenPage();
                          }));
                        },
                      ),
                      InkWell(
                        child: Icon(
                          Icons.network_wifi_sharp,
                          color: Colors.white.withOpacity(0.7),
                        ),
                        onTap: () {},
                      )
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

Widget getCustomStackLayout(
    screenHeight,
    Color backgroundColorSpeed,
    containerText,
    textAssumption,
    Color textColor,
    weatherNumber,
    countryCode) {
  return Stack(
    children: [
      Container(
        margin: EdgeInsets.only(left: 2, right: 2, top: 100),
        // margin: EdgeInsets.only(top: 50),
        height: screenHeight / 7,
        decoration: BoxDecoration(
            // color: Colors.red,
            border: Border.all(width: 2)),
      ),
      Container(
        margin: EdgeInsets.only(left: 25, right: 25),
        height: screenHeight / 4,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(width: 2),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(right: 3),
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          border: Border.all(width: 2),
                          color: backgroundColorSpeed),
                      child: Icon(Icons.speed_outlined,color: Colors.white,size: 25,),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      // crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // color: Colors.pink,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                // color: Colors.red,
                                child: Text(
                                  containerText,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                              ),
                              Container(
                                // color: Colors.black,
                                child: Row(
                                  children: [
                                    Text(
                                      "$weatherNumber ",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    Text("AQI "),
                                    Text(countryCode)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 5),
                          child: Row(
                            children: [
                              Text("Today's air condition is "),
                              Text(
                                "${textAssumption}",
                                style: TextStyle(color: textColor),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Expanded(
                child: MySeparator(
                  color: Colors.black87,
                  height: 2,
                ),
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      // padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        children: [
                          Icon(Icons.device_thermostat_outlined,color: Colors.black.withOpacity(0.5)),
                          Text('24"\u2103"')
                        ],
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        children: [
                          Icon(Icons.water_drop_outlined,color: Colors.black.withOpacity(0.5)),
                          Text("57%")
                        ],
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        children: [
                          Icon(Icons.wb_sunny_outlined,color: Colors.black.withOpacity(0.5)),
                          Text("Light")
                        ],
                      ),
                    ),
                    Container(
                      // padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        children: [
                          Icon(Icons.air_outlined,color: Colors.black.withOpacity(0.5)),
                          Text("13 Km/h")
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      )
    ],
  );
}

class MySeparator extends StatelessWidget {
  const MySeparator({Key? key, this.height = 1, this.color = Colors.black})
      : super(key: key);
  final double height;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        const dashWidth = 10.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
